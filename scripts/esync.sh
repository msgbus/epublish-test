#! /usr/bin/env bash

if [ $# -ne 1 ]; then
	echo "./esync <project>"
	exit
fi

project=$1

make; cp rel/files/app.config rel/$project/etc/app.config; cp ebin/* rel/$project/lib/$project-1/ebin