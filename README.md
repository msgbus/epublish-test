## Overview
---

`epublish` 是一个独立的 publish 模块，调用模块将参数传递给 `epublish` 后，`epublish` 将会完成最后的 publish 动作（包括存储离线消息）。

该模块可以给 erest 和 elogic 项目使用，将 publish 操作从这几个项目结耦出来，方便未来修改 publish 算法和更替模块。

具体的设计文档可见 [设计文档](https://bitbucket.org/msgbus/mbus_docs/wiki/route-table_tfs_aggregation)

目前 `epublish` 提供 3 种 publish 模型：

- **sync publish**

   
      采用上文中设计文档所说的： etopicfs 采用 zset 数据结构和 emqtt 端 batch publish 的方式来进行同步发送；

- **async publish**

  
       采用 Erlang 的 `spawn` API 来进行生成 Erlang 轻量进程实现异步发送逻辑；

- **distributed publish**

   
       采用 erest 中 `erest_dispatcher` 的逻辑，将每个 topic 的分片分散到不同的机器上执行 publish 动作，以此达到集群化 publish 的效果。


除此之外，`epublish` 还加入了限流逻辑，当系统压力过大的时候，会自动终止此次 publish，并返回一个错误。

## How to use
---

目前 `epublish` 模块对外呈现一个 API，如下所示：

```
epublish:publish/1
```

其中 `publish` API 需要一个参数 `PublishArgs`， 这是一个 record，详见 `epublish/include/epublish.hrl` ：

```
-record(publish_args, {
    type,
    broker_score,
    protocol_version,
    uid,
    client_id,
    appkey,
    appid,
    platform,
    message_id,
    qos,
    payload,
    topic,
    opts
}).
```