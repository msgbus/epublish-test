%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 五月 2016 下午4:11
%%%-------------------------------------------------------------------
-author("zhengyinyong").

%%--------------------------------------------------------------------
%% epublish meta
%%--------------------------------------------------------------------
-record(opts, {
    time_to_live,
    platform,
    time_delay,
    location,
    qos = 1,
    apn_json
}).

-record(epublish_args, {
    type,
    node_tag,
    protocol_version,
    uid,
    client_id,
    appkey,
    appid,
    platform,
    message_id,
    qos,
    payload,
    topic,
    offline_msg_ttl,
    msg_queue_ttl,
    opts = #opts{}
}).

-record(publish_throttle, {
    publish_capability = 1000,
    upper_limit_load = 0.8,
    evm_memory_usage_weight = 0.2,
    message_queue_usage_weight = 0.4,
    publish_pool_usage = 0.4,
    max_message_queue_length = 1000,
    system_load_weight = 0.6,
    publish_load_weight = 0.4
}).

-record(epublish_meta, {
    publish_type = sync,
    publish_slice_size = 100,
    async_publish_timeout = 10000,
    dispatch_publish_timeout = 5000,
    max_message_queue_len,
    offline_msg_ttl,
    msg_queue_ttl,
    throttle = #publish_throttle{}
}).

-define(IOS_PLATFORM, 1).

%%--------------------------------------------------------------------
%% rabbitmq proxy
%%--------------------------------------------------------------------
-define(FRONTKEYPREFIX, <<"msgbus_frontend_key_">>).

%%--------------------------------------------------------------------
%% topicfs
%%--------------------------------------------------------------------
-define(TOPICFS_FILE_SLICE_PREFIX,  "FS:/").

%%--------------------------------------------------------------------
%% couchbase key
%%--------------------------------------------------------------------
-define(MSG_QUEUE_PREFIX, <<"msgq_">>).

%%--------------------------------------------------------------------
%% MQTT Packet
%%--------------------------------------------------------------------
-define(QOS_0, 0).
-define(QOS_1, 1).
-define(QOS_2, 2).
