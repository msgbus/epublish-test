%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 06. 五月 2016 上午10:34
%%%-------------------------------------------------------------------
-module(epublish_utils).
-author("zhengyinyong").

-include("epublish.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([make_sure_binary/1, make_sure_string/1, make_sure_integer/1, make_sure_atom/1]).

-export([http_get/1, http_get/2, http_post/2, http_post/3]).

-export([get_env/3, get_env/2, split_list_into_chunk/2, timestamp_in_millisec/0]).

-define(HTTP_TIMEOUT, 10000). %% timeout values are in milliseconds.

make_sure_binary(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

make_sure_string(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
    true ->
        Data
    end.

make_sure_integer(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

make_sure_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

get_env(Application, Module, Arg) ->
    case application:get_env(Application, Module) of
        {ok, ArgsLists} ->
            case proplists:get_value(Arg, ArgsLists) of
                undefined ->
                    undefined;
                Value ->
                    Value
            end
    end.

get_env(Application, Module) ->
    case application:get_env(Application, Module) of
        {ok, Args} ->
            Args;
        _ ->
            undefined
    end.

http_get(URL) ->
    http_get(URL, ?HTTP_TIMEOUT).

http_get(URL, Timeout) ->
    case http_get_request(URL, Timeout) of
        {error, connection_closing} ->
            http_get_request(URL, Timeout);
        Else ->
            Else
    end.

http_post(URL, Content) ->
    http_post(URL, Content, ?HTTP_TIMEOUT).

http_post(URL, Content, Timeout) ->
    case http_post_request(URL, Content, Timeout) of
        {error, connection_closing} ->
            http_post_request(URL, Content, Timeout);
        Else ->
            Else
    end.

split_list_into_chunk(List, Len) ->
    split_list_into_chunk(lists:reverse(List),[],0,Len).
split_list_into_chunk([], Acc, _, _) -> lists:reverse(Acc);
split_list_into_chunk([H|T], Acc, Pos, Max) when Pos == Max ->
    split_list_into_chunk(T, [[H] | Acc], 1, Max);
split_list_into_chunk([H|T],[HAcc|TAcc],Pos,Max) ->
    split_list_into_chunk(T, [[H|HAcc] | TAcc], Pos+1, Max);
split_list_into_chunk([H|T], [], Pos, Max) ->
    split_list_into_chunk(T, [[H]], Pos+1, Max).

timestamp_in_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).

%%%===================================================================
%%% Internal functions
%%%===================================================================
http_get_request(URL, Timeout) ->
    try ibrowse:send_req(URL,[{"accept", "application/json"}],get, [], [], Timeout) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    {ok, Body};
                _Other ->
                    {error, Body}
            end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            {error, REASON}
    catch
        _Type:Error ->
            {error, Error}
    end.

http_post_request(URL, Content, Timeout) ->
    try ibrowse:send_req(URL,[{"content-type", "application/json"}],post, Content, [], Timeout) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    {ok, Body};
                _Other ->
                    {error, Body}
            end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            {error, REASON}
    catch
        _Type:Error ->
            {error, Error}
    end.