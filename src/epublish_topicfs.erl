%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 11. 五月 2016 上午11:11
%%%-------------------------------------------------------------------
-module(epublish_topicfs).
-author("zhengyinyong").

-include("epublish.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([get_topic_size/2, get_topic_range/4]).

%%TODO: 可以改成直接读 redis
get_topic_size(Appkey, Topic) ->
    EtopicfsGetURL = epublish_utils:get_env(epublish, etopicfs, etopicfs_get_url),
    RequestURL1 = epublish_utils:make_sure_string(Appkey) ++ "/"
        ++ epublish_utils:make_sure_string(Topic)
        ++ "/?type=online",
    RequestURL2 = filename:join([RequestURL1]),

    case http_get_from_topicfs(RequestURL2, EtopicfsGetURL) of
        {ok, Body} ->
            try
                Size = epublish_utils:make_sure_integer(jiffy:decode(Body)),
                case Size >= 0 of
                    true ->
                        {ok, Size};
                    false ->
                        {error, invalid_topic_size}
                end
            catch
                _Type:Error  ->
                    {error, Error}
            end;
        Else ->
            Else
    end.

get_topic_range(Appkey, Topic, From, To) ->
    EtopicfsGetURL = epublish_utils:get_env(epublish, etopicfs, etopicfs_get_url),
    RequestURL1 = epublish_utils:make_sure_string(Appkey) ++ "/"
        ++ epublish_utils:make_sure_string(Topic)
        ++ "/?type=range&from=" ++ epublish_utils:make_sure_string(From) ++ "&to=" ++ epublish_utils:make_sure_string(To),
    RequestURL2 = filename:join([RequestURL1]),

    case http_get_from_topicfs(RequestURL2, EtopicfsGetURL) of
        {ok, Body} ->
            try
                UidLists = jiffy:decode(Body),
                case is_ios_topic(Topic) of
                    true ->
                        {ok, UidLists};
                    false ->
                        {ok, format_uid_lists(UidLists)}
                end
            catch
                _Type:Error  ->
                    {error, Error}
            end;
        Else ->
            Else
    end.

%%%===================================================================
%%% Internal functions
%%%===================================================================
http_get_from_topicfs(RequestURL, EtopicfsGetURL) ->
    case epublish_utils:http_get(lists:append(EtopicfsGetURL, RequestURL)) of
        {ok, Body} ->
            {ok, Body};
        Else ->
            Else
    end.

format_uid_lists(UidLists) ->
    UidLists2 = epublish_utils:split_list_into_chunk(UidLists, 2),
    Result = lists:foldl(fun([Uid, BrokerScore], Dict) ->
                            dict:append(BrokerScore, Uid, Dict)
                         end, dict:new(), UidLists2),
    dict:to_list(Result).

is_ios_topic(Topic) ->
    lists:suffix("/" ++ ?IOS_PLATFORM, epublish_utils:make_sure_string(Topic)).
