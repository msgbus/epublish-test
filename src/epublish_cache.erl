%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 19. 七月 2016 下午2:32
%%%-------------------------------------------------------------------
-module(epublish_cache).
-author("zhengyinyong").

%% API
-export([init/1, get/2, set/3]).

init(EtsCacheLists) ->
    [ets:new(EtsCache, [set, public, named_table]) || EtsCache <- EtsCacheLists].

get(EtsCache, Key) ->
    case ets:lookup(EtsCache, Key) of
        [] ->
            none;
        [{Key, Value}] ->
            Value
    end.

set(EtsCache, Key, Value) ->
    ets:insert(EtsCache, {Key, Value}).
