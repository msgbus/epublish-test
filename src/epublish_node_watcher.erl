%%%-------------------------------------------------------------------
%%% @author zy
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 05. 一月 2016 5:25 PM
%%%-------------------------------------------------------------------
-module(epublish_node_watcher).
-author("zy").

-behaviour(gen_server).

-compile({parse_transform, lager_transform}).

%% API
-export([start_link/0, join_cluster/1, leave_cluster/0, nodes/0,
         is_up/1, peers/0, best_node/0, pause/0, resume/0, remove_node/1]).

%% gen_server callbacks
-export([init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-define(SERVER, ?MODULE).

-define(PEER_FILE, "./peer.data").

-record(state, {
    bcast_tref = undefined,
    status = up,
    peers = []
}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

join_cluster(Node) ->
    case net_adm:ping(Node) of
        pong ->
            {Replies, _BadNodes} = gen_server:multi_call(erlang:nodes(), ?SERVER, {add_node, node()}),
            lists:any(
                fun({_Node, Reply}) ->
                    case Reply of
                        {ok, AllNodes} ->
                            gen_server:cast(?SERVER, {add_nodes, AllNodes}),
                            true;
                        _ ->
                            false
                    end
                end, Replies);
        _ ->
            not_started
    end.

leave_cluster() ->
    gen_server:call(?SERVER, {leave_cluster}).

nodes() ->
    ets:tab2list(?MODULE).

is_up(Node) ->
    ets:member(?MODULE, Node).

peers() ->
    gen_server:call(?SERVER, {get_peers}).

best_node() ->
    AllNodes = ets:tab2list(?MODULE),
    Sum = lists:foldl(fun({_Node, _TS, Weight}, S) -> Weight + S end, 0, AllNodes),
    get_best_node(AllNodes, random:uniform() * Sum).
get_best_node([], _) ->
    node();
get_best_node([{Node, _, _}], _) ->
    Node;
get_best_node([{Node, _, Weight} | Nodes], Rnd) ->
    case Rnd - Weight of
        NewRnd when NewRnd < 0 ->
            Node;
        NewRnd ->
            get_best_node(Nodes, NewRnd)
    end.

pause() ->
    gen_server:call(?SERVER, {pause}).

resume() ->
    gen_server:call(?SERVER, {resume}).

remove_node(Node) ->
    gen_server:call(?SERVER, {remove_node, Node}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term()} | ignore).
init([]) ->
    %% Watch for node up/down events
    ok = net_kernel:monitor_nodes(true),

    %% Setup ETS table to track node status
    ?MODULE = ets:new(?MODULE, [protected, {read_concurrency, true}, named_table]),

    {ok, State2} = file2peer(#state{}),
    {ok, schedule_broadcast(State2)}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
        State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}} |
    {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_call({get_peers}, _From, State) ->
    {reply, {ok, State#state.peers}, State};

handle_call({add_node, _Node}, _From, State = #state{status = leave}) ->
    {reply, leave, State};
handle_call({add_node, Node}, _From, State) ->
    {Result, State2} = add_node(Node, State),
    {reply, {Result, State2#state.peers}, State2};

handle_call({pause}, _From, State = #state{status = leave}) ->
    {reply, leave, State};
handle_call({pause}, _From, State) ->
    {reply, ok, State#state{status = down}};

handle_call({resume}, _From, State = #state{status = leave}) ->
    {reply, leave, State};
handle_call({resume}, _From, State) ->
    {reply, ok, State#state{status = up}};

handle_call({leave_cluster}, _From, State) ->
    peer2file(State#state{peers = [erlang:node()]}),
    {reply, ok, State#state{status = leave}};

handle_call({remove_node, _Node}, _From, State = #state{status = leave}) ->
    {reply, leave, State};
handle_call({remove_node, Node}, _From, State = #state{peers = Peers0}) ->
    case ets:member(?MODULE, Node) of
        true ->
            {reply, {error, node_is_up}, State};
        false ->
            case lists:member(Node, Peers0) of
                true ->
                    {ok, State2} = peer2file(State#state{peers = lists:delete(Node, Peers0)}),
                    {reply, ok, State2};
                false ->
                    {reply, ok, State}
            end
    end;

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_cast({up, _FreeScheduler, _Node}, State = #state{status = leave}) ->
    {noreply, State};
handle_cast({up, FreeScheduler, Node}, State) ->
    State2 = node_up(Node, FreeScheduler, State),
    {noreply, State2};

handle_cast({down, _Node}, State = #state{status = leave}) ->
    {noreply, State};
handle_cast({down, Node}, State) ->
    State2 = node_down(Node, State),
    {noreply, State2};

handle_cast({leave, _Node}, State = #state{status = leave}) ->
    {noreply, State};
handle_cast({leave, Node}, State = #state{peers = Peers0}) ->
    State2 = node_down(Node, State),
    case lists:member(Node, Peers0) of
        true ->
            {ok, State3} = peer2file(State2#state{peers = lists:delete(Node, Peers0)}),
            {noreply, State3};
        _ ->
            {noreply, State2}
    end;

handle_cast({add_nodes, _Nodes}, State = #state{status = leave}) ->
    {noreply, State};
handle_cast({add_nodes, Nodes}, State) ->
    State2 = lists:foldl(
        fun(Node, S) ->
            {_, S2} = add_node(Node, S),
            S2
        end, State, Nodes),
    {noreply, State2};

handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info({nodeup, _Node}, State) ->
    {noreply, State};
handle_info({nodedown, Node}, State) ->
    State2 = node_down(Node, State),
    {noreply, State2};

handle_info(broadcast, State = #state{peers = Peers}) ->
    State2 = broadcast(Peers, State),
    {noreply, State2};

handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
        State :: #state{}) -> term()).
terminate(_Reason, State = #state{peers = Peers}) ->
    broadcast(Peers, State#state { status = down }),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
        Extra :: term()) ->
    {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
add_node(Node, State = #state{peers = Peers0}) ->
    case lists:member(Node, Peers0) of
        false when Node =/= node() ->
            {Result, State2} = peer2file(State#state{peers = [Node | Peers0]}),
            {Result, State2};
        _ ->
            {ok, State}
    end.

node_up(Node, FreeScheduler, State) ->
    S2 = case is_up(Node) of
             false ->
                 lager:info("Node ~p up", [Node]),
                 {_, State2} = add_node(Node, State),
                 broadcast([Node], State2);
             true ->
                 State
         end,

    node_update(Node, FreeScheduler, S2).

node_update(Node, FreeScheduler, State) ->
    Now = epublish_utils:timestamp_in_millisec(),
    ets:insert(?MODULE, {Node, Now, FreeScheduler}),
    State.

node_down(Node, State) ->
    lager:info("Node ~p down", [Node]),
    ets:delete(?MODULE, Node),
    State.

schedule_broadcast(State) ->
    case (State#state.bcast_tref) of
        undefined ->
            ok;
        OldTref ->
            _ = erlang:cancel_timer(OldTref),
            ok
    end,
    Interval = application:get_env(epublish, node_watcher_interval, 1000),
    Tref = erlang:send_after(Interval, self(), broadcast),
    State#state{bcast_tref = Tref}.

broadcast(Nodes, State) ->
    case (State#state.status) of
        up ->
            {_StateName, UnUse, _, InUse} = poolboy:status(epublish_dispatcher_pool),
            Avg = UnUse / (UnUse + InUse),
            FreeScheduler =
                case Avg of
                    V when V > 0.5 ->
                        0.6 * V + 0.2;
                    V ->
                        0.4 * V + 0.3
                end,
            Msg = {up, FreeScheduler, node()};
        down ->
            Msg = {down, node()};
        leave ->
            Msg = {leave, node()}
    end,
    gen_server:abcast(Nodes, ?MODULE, Msg),
    schedule_broadcast(State).

file2peer(State) ->
    case filelib:last_modified(?PEER_FILE) of
        0 ->
            {ok, State#state{peers = [erlang:node()]}};
        _ ->
            case file:read_file(?PEER_FILE) of
                {ok, Binary} ->
                    {ok, State#state{peers = binary_to_term(Binary)}};
                {error, Reason} ->
                    {error, Reason}
            end
    end.

peer2file(State) ->
    ok = filelib:ensure_dir(?PEER_FILE),
    try
        ok = replace_file(?PEER_FILE, term_to_binary(State#state.peers)),
        {ok, State}
    catch
        _:Err ->
            lager:error("Unable to write peer to \"~s\" - ~p\n", [?PEER_FILE, Err]),
            {error, Err}
    end.

%% @doc Atomically/safely (to some reasonable level of durablity)
%% replace file `FN' with `Data'. NOTE: since 2.0.3 semantic changed
%% slightly: If `FN' cannot be opened, will not error with a
%% `badmatch', as before, but will instead return `{error, Reason}'
-spec replace_file(string(), iodata()) -> ok | {error, term()}.
replace_file(FN, Data) ->
    TmpFN = FN ++ ".tmp",
    case file:open(TmpFN, [write, raw]) of
        {ok, FH} ->
            try
                ok = file:write(FH, Data),
                ok = file:sync(FH),
                ok = file:close(FH),
                ok = file:rename(TmpFN, FN),
                {ok, Contents} = read_file(FN),
                true = (Contents == iolist_to_binary(Data)),
                ok
            catch _:Err ->
                {error, Err}
            end;
        Err ->
            Err
    end.

%% @doc Similar to {@link file:read_file/1} but uses raw file `I/O'
read_file(FName) ->
    {ok, FD} = file:open(FName, [read, raw, binary]),
    IOList = read_file(FD, []),
    ok = file:close(FD),
    {ok, iolist_to_binary(IOList)}.

read_file(FD, Acc) ->
    case file:read(FD, 4096) of
        {ok, Data} ->
            read_file(FD, [Data|Acc]);
        eof ->
            lists:reverse(Acc)
    end.