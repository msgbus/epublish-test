%%%-------------------------------------------------------------------
%%% @author zy
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 11. 一月 2016 10:30 AM
%%%-------------------------------------------------------------------
-module(epublish_dispatcher).
-author("zy").

-behaviour(gen_server).

%% API
-export([transaction/1, transaction/2, transaction/3, status/0, start_link/1]).

%% gen_server callbacks
-export([init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-define(SERVER, ?MODULE).
-define(TIMEOUT, 5000).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================
transaction(Fun) ->
    transaction(Fun, ?TIMEOUT).

transaction(Fun, Timeout) ->
    BestNode = epublish_node_watcher:best_node(),
    transaction(BestNode, Fun, Timeout).

transaction(Node, Fun, Timeout) ->
    case poolboy:checkout({epublish_dispatcher_pool, Node}, false) of
        full ->
            {error, unavailable};
        Worker ->
            case catch gen_server:call(Worker, {transaction, Fun}, Timeout) of
                {'EXIT', {timeout, _}} ->
                    exit(Worker, kill),
                    {error, timeout};
                {'EXIT', Reason} ->
                    ok = poolboy:checkin({epublish_dispatcher_pool, Node}, Worker),
                    {error, Reason};
                Result ->
                    ok = poolboy:checkin({epublish_dispatcher_pool, Node}, Worker),
                    Result
            end
    end.

status() ->
    AllNodes = epublish_node_watcher:nodes(),
    lists:foldl(
        fun({Node, _TS, _Weight}, Acc) ->
            {StateName, _, _, InUse} = poolboy:status({epublish_dispatcher_pool, Node}),
            [{Node, StateName, InUse} | Acc]
        end,
        [], AllNodes).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link(Args :: term()) ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link(_Args) ->
    gen_server:start_link(?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term()} | ignore).
init([]) ->
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
        State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}} |
    {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_call({transaction, Fun}, _From, State) ->
    Ret = Fun(),
    {reply, Ret, State};

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
        State :: #state{}) -> term()).
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
        Extra :: term()) ->
    {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
