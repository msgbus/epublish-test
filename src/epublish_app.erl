-module(epublish_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    set_ibrowse_max_sessions_for_etopicfs(),
    epublish_cache:init([broker_score_to_nodetag]),
    epublish_sup:start_link().

stop(_State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
set_ibrowse_max_sessions_for_etopicfs() ->
    ArgsForGet = epublish_utils:get_env(epublish, etopicfs, etopicfs_ibrowse_settings_for_get),
    ibrowse:set_max_sessions(proplists:get_value(host, ArgsForGet),
                             proplists:get_value(port, ArgsForGet),
                             proplists:get_value(max_sessions, ArgsForGet)).