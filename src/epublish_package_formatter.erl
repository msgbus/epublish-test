%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 22. 五月 2016 上午11:53
%%%-------------------------------------------------------------------
-module(epublish_package_formatter).
-author("zhengyinyong").

-include("epublish.hrl").
-include_lib("msgbus_common_utils/include/internal_package_pb.hrl").

%% API
-export([form_publish_internal_package/4]).

-define(PUBLISH, 3).
-define(HIGHBIT, 2#10000000).
-define(LOWBITS, 2#01111111).
-define(YUNBA_MQTT_VERSION, 16#13).

%%%===================================================================
%%% Internal Package Formatter
%%%===================================================================
form_publish_internal_package(NodeTag, PublishUidLists, PublishArgs, _EpublishMeta) ->
    ProtocolVersion = PublishArgs#epublish_args.protocol_version,
    FromUid = PublishArgs#epublish_args.uid,
    Appkey = PublishArgs#epublish_args.appkey,
    MqttPackageBytes = form_publish_mqtt_package(PublishArgs),
    PublishUidLists2 = lists:map(fun(Uid) -> epublish_utils:make_sure_integer(Uid) end, PublishUidLists),
    InternalPackage = #internalpackage {
        from_ip = 0, from_port = 0,
        to_ip = 0, to_port = 0,
        from_tag = epublish_utils:make_sure_string(NodeTag), protocol_version = ProtocolVersion,
        uid = epublish_utils:make_sure_integer(FromUid), to_uid = PublishUidLists2, appkey = Appkey,
        client_id = "", mqtt_package = MqttPackageBytes},
    InternalPackage.

%%%===================================================================
%%% Internal functions
%%%===================================================================
form_publish_mqtt_package(PublishArgs) ->
    Topic = epublish_utils:make_sure_string(PublishArgs#epublish_args.topic),
    MessageId = PublishArgs#epublish_args.message_id,
    QoS = PublishArgs#epublish_args.qos,
    Payload = epublish_utils:make_sure_binary(PublishArgs#epublish_args.payload),
    ProtocolVersion = PublishArgs#epublish_args.protocol_version,
    Dup = 0,
    Retain = 0,

    MessageIdLen = message_id_len(ProtocolVersion),
    TopicBin = serialise_utf(Topic),
    MessageIdBin = case QoS of
                       ?QOS_0 -> <<>>;
                       ?QOS_1 -> <<MessageId:MessageIdLen/big>>;
                       ?QOS_2 -> <<MessageId:MessageIdLen/big>>
                   end,

    Variable = <<TopicBin/binary, MessageIdBin/binary>>,

    LengthBin = serialise_len(size(Variable) + size(Payload)),

    MqttPackageBytes =
        <<?PUBLISH:4, Dup:1, QoS:2, Retain:1, LengthBin/binary, Variable/binary, Payload/binary>>,

    MqttPackageBytes.

message_id_len(ProtocolVersion) ->
    MessageIdLen =
        if
            ProtocolVersion == ?YUNBA_MQTT_VERSION ->
                64;
            true ->
                16
        end,
    MessageIdLen.

serialise_utf(String) ->
    StringBin = unicode:characters_to_binary(String),
    Len = size(StringBin),
    true = (Len =< 16#ffff),
    <<Len:16/big, StringBin/binary>>.

serialise_len(N) when N =< ?LOWBITS ->
    <<0:1, N:7>>;
serialise_len(N) ->
    <<1:1, (N rem ?HIGHBIT):7, (serialise_len(N div ?HIGHBIT))/binary>>.
