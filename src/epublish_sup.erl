-module(epublish_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).
-define(SUP_CHILD(I, Args), {I, {I, start_link, Args}, permanent, infinity, supervisor, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    RestartStrategy = epublish_utils:get_env(epublish, general_supervisor, restart_strategy),
    MaxRestarts = epublish_utils:get_env(epublish, general_supervisor, max_restarts),
    MaxSecondsBetweenRestarts = epublish_utils:get_env(epublish, general_supervisor, max_seconds_between_restarts),

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Process = [?CHILD(epublish_node_watcher, worker)] ++ generate_poolboy_child_spec(),

    {ok, {SupFlags, Process} }.

%%%===================================================================
%%% Internal functions
%%%===================================================================
generate_poolboy_child_spec() ->
    {ok, Pools} = application:get_env(epublish, pools),
    PoolSpecs = lists:map(fun({Name, Args}) ->
                    WorkerModule = proplists:get_value(worker_module, Args),
                    SizeArgs = proplists:get_value(size_args, Args),
                    WorkerArgs = proplists:get_value(worker_args, Args),
                    PoolArgs = [{name, {local, Name}}, {worker_module, WorkerModule}] ++ SizeArgs,
                          poolboy:child_spec(Name, PoolArgs, WorkerArgs)
                          end, Pools),
    PoolSpecs.
