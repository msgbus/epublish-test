%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 12. 五月 2016 下午4:43
%%%-------------------------------------------------------------------
-module(epublish_storage).
-author("zhengyinyong").

-include("epublish.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([save_message_body/2, save_msgid_list/4, get_node_tag/1]).

save_message_body(PublishArgs, EpublishMeta) ->
    AppidBin = epublish_utils:make_sure_binary(PublishArgs#epublish_args.appid),
    MessageIdBin = epublish_utils:make_sure_binary(PublishArgs#epublish_args.message_id),

    Body = {
        [
            {<<"appkey">>, epublish_utils:make_sure_binary(PublishArgs#epublish_args.appkey)},
            {<<"topic">>, epublish_utils:make_sure_binary(PublishArgs#epublish_args.topic)},
            {<<"payload">>, epublish_utils:make_sure_binary(PublishArgs#epublish_args.payload)},
            {<<"qos">>, epublish_utils:make_sure_binary(PublishArgs#epublish_args.qos)},
            {<<"publisher">>, epublish_utils:make_sure_binary(PublishArgs#epublish_args.uid)},
            {<<"opts">>, epublish_utils:make_sure_binary(PublishArgs#epublish_args.opts)},
            {<<"timestamp">>, epublish_utils:make_sure_binary(epublish_utils:timestamp_in_millisec())}
        ]
    },
    EncodeMessageBody = jiffy:encode(Body, [force_utf8]),

    TTL = case PublishArgs#epublish_args.offline_msg_ttl of
              undefined ->
                  EpublishMeta#epublish_meta.offline_msg_ttl;
              TTL1 ->
                  TTL1
          end,

    Key = <<"pub_", AppidBin/binary, "_", MessageIdBin/binary>>,
    cbclient_storage:set(meta, Key, EncodeMessageBody, TTL).

save_msgid_list(Uid, MessageId, PublishArgs, EpublishMeta) ->
    MaxMessageQueueLen = proplists:get_value(max_message_queue_len, epublish_utils:get_env(cbclient, couchbase, msg_queue)),
    Key = iolist_to_binary([epublish_utils:make_sure_binary(?MSG_QUEUE_PREFIX), epublish_utils:make_sure_binary(Uid)]),

    TTL = case PublishArgs#epublish_args.msg_queue_ttl of
              undefined ->
                  EpublishMeta#epublish_meta.msg_queue_ttl;
              TTL1 ->
                  TTL1
          end,

    case cbclient_storage:lenqueue(msg_queue, Key, epublish_utils:make_sure_integer(MessageId), MaxMessageQueueLen) of
        ok ->
            case TTL of
                undefined ->
                    ok;
                _ ->
                    cbclient_storage:touch(msg_queue, Key, TTL)
            end;
        _Else -> %% ignore error
            ok
    end.

get_node_tag(BrokerScore) ->
    try
        {ok, BrokerScoreRegulation} = application:get_env(epublish, broker_score_regulation),
        {host_provider_code, PredefinedHostProviderCode} = lists:keyfind(host_provider_code, 1, BrokerScoreRegulation),
        {zone_code, PredefinedZoneCode} = lists:keyfind(zone_code, 1, BrokerScoreRegulation),
        BrokerScoreBin = binary:encode_unsigned(epublish_utils:make_sure_integer(BrokerScore)),
        BrokerScoreBin1 = lists:foldl(fun(_X, Bin) -> <<16#00, Bin/binary>> end, BrokerScoreBin, lists:seq(1, 9 - size(BrokerScoreBin))),
        <<HostProviderCode:8, ZoneCode:8, Customer:32, FrontSeq:16, _Reserved:8>> = BrokerScoreBin1,
        {HostProviderName, HostProviderCode} = lists:keyfind(HostProviderCode, 2, PredefinedHostProviderCode),
        {ZoneCodeName, ZoneCode} = lists:keyfind(ZoneCode, 2, PredefinedZoneCode),
        CustomerName = case binary_to_list(binary:encode_unsigned(Customer)) of
                           "comm" -> "";
                           Else -> Else
                       end,
        NodeTag = epublish_utils:make_sure_binary(HostProviderName ++ ZoneCodeName ++ "-front-" ++ CustomerName ++ integer_to_list(FrontSeq)),
        epublish_cache:set(broker_score_to_nodetag, BrokerScore, NodeTag),
        NodeTag
    catch
        _Type:Error ->
            lager:critical("runtime error:~p, generate node tag for ~p failed !", [Error, BrokerScore]),
            {error, run_time_error}
    end.
%%%===================================================================
%%% Internal functions
%%%===================================================================
