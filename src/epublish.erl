%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 14. 六月 2016 下午12:16
%%%-------------------------------------------------------------------
-module(epublish).
-author("zhengyinyong").

%% API
-include("epublish.hrl").

-compile({parse_transform, lager_transform}).

-export([publish/1]).

publish(PublishArgs) ->
    EpublishMeta = get_epublish_meta(),
    case PublishArgs#epublish_args.type of
        publish_to_topic ->
            publish_to_topic_with_qos(PublishArgs, EpublishMeta);
        publish_to_one_uid ->
            publish_to_one_uid(PublishArgs, EpublishMeta);
        publish_by_msgid ->
            publish_by_msgid(PublishArgs, EpublishMeta);
        _Others ->
            ok
    end.

publish_to_topic_with_qos(PublishArgs, EpublishMeta) ->
    QoS = PublishArgs#epublish_args.qos,
    Result1 = case QoS > 0 of
                  true ->
                      publish_to_topic_greater_qos0(PublishArgs, EpublishMeta);
                  false ->
                      publish_to_topic_qos0(PublishArgs, EpublishMeta)
              end,
    publish_to_apns(PublishArgs, EpublishMeta),
    Result1.

publish_to_topic_qos0(PublishArgs, EpublishMeta) ->
    sync_publish(PublishArgs, EpublishMeta).

publish_to_topic_greater_qos0(PublishArgs, EpublishMeta) ->
    case epublish_storage:save_message_body(PublishArgs, EpublishMeta) of
        ok ->
            case EpublishMeta#epublish_meta.publish_type of
                sync ->
                    sync_publish(PublishArgs, EpublishMeta);
                async ->
                    async_publish(PublishArgs, EpublishMeta);
                distributed ->
                    distributed_publish(PublishArgs, EpublishMeta)
            end;
        Result ->
            lager:error("save message body failed, result:~p~n ", [Result]),
            {error, save_message_body_failed}
    end.

publish_to_one_uid(PublishArgs, EpublishMeta) ->
    Uid = PublishArgs#epublish_args.uid,
    NodeTag = PublishArgs#epublish_args.node_tag,
    InternalPackage = epublish_package_formatter:form_publish_internal_package(NodeTag, [Uid], PublishArgs, EpublishMeta),
    send_to_front2(NodeTag, InternalPackage),
    case PublishArgs#epublish_args.platform of
        ?IOS_PLATFORM ->
            send_apns_by_uid(Uid, PublishArgs, EpublishMeta);
        _ ->
            ignore
    end,
    ok.

publish_by_msgid(_PublishArgs, _EpublishMeta) ->
    ok.

publish_to_apns(PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic ++ "/" ++ epublish_utils:make_sure_string(?IOS_PLATFORM),
    SliceSize = EpublishMeta#epublish_meta.publish_slice_size,
    case epublish_topicfs:get_topic_size(Appkey, Topic) of
        {ok, TopicSize} ->
            case throttle_for_publish(TopicSize, EpublishMeta) of
                true ->
                    SliceNum = get_slice_num(TopicSize, SliceSize),
                    TopicSliceLists = generate_topic_slice_lists(SliceNum, TopicSize, SliceSize),
                    Result = lists:map(fun({From, To}) ->
                                            publish_to_apns_topic_slice(From, To, PublishArgs, EpublishMeta)
                                       end, TopicSliceLists),
                    check_publish_result(SliceNum, Result);
                false ->
                    {error, system_over_load}
            end;
        _Error ->
            {error, get_topic_size_failed}
    end.

%%%===================================================================
%%% Internal functions
%%%===================================================================
sync_publish(PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic,
    SliceSize = EpublishMeta#epublish_meta.publish_slice_size,
    case epublish_topicfs:get_topic_size(Appkey, Topic) of
        {ok, TopicSize} ->
            case throttle_for_publish(TopicSize, EpublishMeta) of
                true ->
                    SliceNum = get_slice_num(TopicSize, SliceSize),
                    TopicSliceLists = generate_topic_slice_lists(SliceNum, TopicSize, SliceSize),
                    Result = lists:map(fun({From, To}) ->
                                           publish_to_topic_slice(From, To, PublishArgs, EpublishMeta)
                                       end, TopicSliceLists),
                    check_publish_result(SliceNum, Result);
                false ->
                    {error, system_over_load}
            end;
        _Error ->
            {error, get_topic_size_failed}
    end.

async_publish(PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic,
    SliceSize = EpublishMeta#epublish_meta.publish_slice_size,
    AsyncPublishTimeout = EpublishMeta#epublish_meta.async_publish_timeout,
    case epublish_topicfs:get_topic_size(Appkey, Topic) of
        {ok, TopicSize} ->
            case throttle_for_publish(TopicSize, EpublishMeta) of
                true ->
                    Parent = self(),
                    SliceNum = get_slice_num(TopicSize, SliceSize),
                    TopicSliceLists = generate_topic_slice_lists(SliceNum, TopicSize, SliceSize),
                    [spawn_link(fun() ->
                        async_publish_to_topic_slice(Parent, From, To, PublishArgs, EpublishMeta)
                                end) || {From, To} <- TopicSliceLists],
                    case wait_for_async_publish(lists:duplicate(length(TopicSliceLists), ok), [], AsyncPublishTimeout) of
                        async_publish_finished ->
                            ok;
                        _Else ->
                            {error, async_publish_failed}
                    end;
                false ->
                    {error, system_over_load}
            end;
        _Error ->
            {error, get_topic_size_failed}
    end.

distributed_publish(PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic,
    SliceSize = EpublishMeta#epublish_meta.publish_slice_size,
    case epublish_topicfs:get_topic_size(Appkey, Topic) of
        {ok, TopicSize} ->
            case throttle_for_publish(TopicSize, EpublishMeta) of
                true ->
                    SliceNum = get_slice_num(TopicSize, SliceSize),
                    TopicSliceLists = generate_topic_slice_lists(SliceNum, TopicSize, SliceSize),
                    Result = lists:map(fun({From, To}) ->
                                            distributed_publish_to_topic_slice(From, To, PublishArgs, EpublishMeta)
                                       end, TopicSliceLists),
                    check_publish_result(SliceNum, Result);
                false ->
                    {error, system_over_load}
            end;
        _Else ->
            {error, get_file_slice_failed}
    end.

publish_to_topic_slice(From, To, PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic,
    case epublish_topicfs:get_topic_range(Appkey, Topic, From, To) of
        {ok, ScoredUidLists} ->
            Result = lists:map(fun({BrokerScore, PublishUidLists}) ->
                            batch_publish_by_score(BrokerScore, PublishUidLists, PublishArgs, EpublishMeta)
                      end, ScoredUidLists),
            check_publish_result(length(ScoredUidLists), Result);
        _Else ->
            {error, get_uid_list_failed}
    end.

publish_to_apns_topic_slice(From, To, PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic,
    case epublish_topicfs:get_topic_range(Appkey, Topic, From, To) of
        {ok, UidLists} ->
            Result = lists:map(fun(Uid) ->
                                    send_apns_by_uid(Uid, PublishArgs, EpublishMeta)
                               end, UidLists),
            check_publish_result(length(UidLists), Result);
        _Else ->
            {error, get_uid_list_failed}
    end.

send_apns_by_uid(Uid, PublishArgs, _EpublishMeta) ->
    Now = 0,
    Unkown = 0,
    Ver = 2,
    Uid1 = epublish_utils:make_sure_integer(Uid),
    Opts = PublishArgs#epublish_args.opts,
    Payload = epublish_utils:make_sure_binary(PublishArgs#epublish_args.payload),
    APNsPackage = case Opts of
                  undefined ->
                      PayloadLen = byte_size(Payload),
                      <<Uid1:64, Now:32, PayloadLen:16, Payload/binary>>;
                  _ ->
                      case Opts#opts.apn_json of
                          undefined ->
                              ignore;
                          _ ->
                              APNJsonBin = epublish_utils:make_sure_binary(PublishArgs#epublish_args.opts#opts.apn_json),
                              Len = byte_size(APNJsonBin),
                              <<Uid1:64, Now:32, Unkown:16, Ver:8, Len:16, APNJsonBin/binary>>
                      end
              end,
    case APNsPackage of
        ignore ->
            ok;
        _ ->
            send_to_apns(APNsPackage)
    end.

send_to_apns(ApnsPackage) ->
    RoutingKey = <<"apns_routing_key">>,
    msgbus_amqp_proxy:send(RoutingKey, ApnsPackage).

batch_publish_by_score(BrokerScore, PublishUidLists, PublishArgs, EpublishMeta) ->
    InternalPackage = epublish_package_formatter:form_publish_internal_package(BrokerScore, PublishUidLists, PublishArgs, EpublishMeta),
    save_msg_list(PublishUidLists, PublishArgs, EpublishMeta),
    send_to_front(BrokerScore, InternalPackage),
    ok.

async_publish_to_topic_slice(Parent, From, To, PublishArgs, EpublishMeta) ->
    Appkey = PublishArgs#epublish_args.appkey,
    Topic = PublishArgs#epublish_args.topic,
    case epublish_topicfs:get_topic_range(Appkey, Topic, From, To) of
        {ok, ScoredUidLists} ->
            Result = lists:map(fun({BrokerScore, PublishUidLists}) ->
                batch_publish_by_score(BrokerScore, PublishUidLists, PublishArgs, EpublishMeta)
                               end, ScoredUidLists),
            case check_publish_result(Result, length(ScoredUidLists)) of
                ok ->
                    Parent ! {ok, self()};
                {error, Reason} ->
                    {error, Reason}
            end;
        _Else ->
            {error, get_topic_range_failed}
    end.

wait_for_async_publish(ExpectedResult, ResultCollector, Timeout) ->
    receive
        {ok, _Pid} ->
            ResultCollector2 = ResultCollector ++ [ok],
            case ResultCollector2 /= ExpectedResult of
                true ->
                    wait_for_async_publish(ExpectedResult, ResultCollector2, Timeout);
                false ->
                    async_publish_finished
            end;
        _Else ->
            {error, async_publish_failed}
    after Timeout ->
        {error, async_publish_failed}
    end.

distributed_publish_to_topic_slice(From, To, PublishArgs, EpublishMeta) ->
    publish_to_topic_by_dispatcher(From, To, PublishArgs, EpublishMeta).

publish_to_topic_by_dispatcher(From, To, PublishArgs, EpublishMeta) ->
    BestNode = epublish_node_watcher:best_node(),
    Result =
        case epublish_dispatcher:transaction(
            BestNode,
            fun() ->
                publish_to_topic_slice(From, To, PublishArgs, EpublishMeta)
            end,
            EpublishMeta#epublish_meta.dispatch_publish_timeout)
        of
            {error, Reason} ->
                lager:error("publish_to_topic_slice failed, From = ~p, To = ~p, Reason = ~p~n", [From, To, Reason]),
                {error, Reason};
            ok ->
                ok
        end,
    Result.

%% TODO: 限流操作应该做成一个 module
throttle_for_publish(TopicSize, EpublishMeta) ->
    SystemLoadWeight = EpublishMeta#epublish_meta.throttle#publish_throttle.system_load_weight,
    PublishLoadWeight = EpublishMeta#epublish_meta.throttle#publish_throttle.publish_load_weight,
    UpperLimitLoad = EpublishMeta#epublish_meta.throttle#publish_throttle.upper_limit_load,

    CurrentSystemLoad = calculate_system_load(EpublishMeta),
    PublishLoad = calculate_publish_load(TopicSize, EpublishMeta),

    Load = CurrentSystemLoad * SystemLoadWeight + PublishLoad * PublishLoadWeight,
    lager:debug("Load = ~p, CurrentSystemLoad = ~p, PublishLoad = ~p~n", [Load, CurrentSystemLoad, PublishLoad]),
    Load =< UpperLimitLoad.

calculate_system_load(EpublishMeta) ->
    EvmMemoryUsageWeight = EpublishMeta#epublish_meta.throttle#publish_throttle.evm_memory_usage_weight,
    PublishPoolUsageWeight = EpublishMeta#epublish_meta.throttle#publish_throttle.publish_pool_usage,
    MessageQueueUsageWeight = EpublishMeta#epublish_meta.throttle#publish_throttle.message_queue_usage_weight,
    MaxMessageQueueLength = EpublishMeta#epublish_meta.throttle#publish_throttle.max_message_queue_length,

    {_PoolState, StandbyWorkersNum, _Overflow, WorkingWokers} = poolboy:status(epublish_dispatcher_pool),
    PoolUsage = WorkingWokers / (WorkingWokers + StandbyWorkersNum ),
    EvmMemoryUsage = recon_alloc:memory(usage),

    {message_queue_len, MessageQueueLength} = erlang:process_info(self(), message_queue_len),
    MessageQueueUsage = MessageQueueLength / MaxMessageQueueLength,

    lager:debug("PoolUsage = ~p, EvmMemoryUsage = ~p, MessageQueueUsage = ~p~n", [PoolUsage, EvmMemoryUsage, MessageQueueUsage]),

    EvmMemoryUsageWeight * EvmMemoryUsage + PublishPoolUsageWeight * PoolUsage + MessageQueueUsage * MessageQueueUsageWeight.

calculate_publish_load(TopicSize, EpublishMeta) ->
    PublishCapability = EpublishMeta#epublish_meta.throttle#publish_throttle.publish_capability,
    PublishLoad = TopicSize / PublishCapability,
    PublishLoad.

send_to_front(BrokerScore, InternalPackage) ->
    Nodetag = epublish_storage:get_node_tag(epublish_utils:make_sure_integer(BrokerScore)),
    RoutingKey = << ?FRONTKEYPREFIX/binary, Nodetag/binary >>,
    BytesWithHeader = internal_package_pb:encode_internalpackage(InternalPackage),
    msgbus_amqp_proxy:send(RoutingKey, epublish_utils:make_sure_binary(BytesWithHeader)).

send_to_front2(NodeTag, InternalPackage) ->
    NodeTag1 = epublish_utils:make_sure_binary(NodeTag),
    RoutingKey = << ?FRONTKEYPREFIX/binary, NodeTag1/binary >>,
    BytesWithHeader = internal_package_pb:encode_internalpackage(InternalPackage),
    msgbus_amqp_proxy:send(RoutingKey, epublish_utils:make_sure_binary(BytesWithHeader)).

%%TODO: 取环境变量的参数，需要细化参数
get_epublish_meta() ->
    #epublish_meta {
        publish_type = epublish_utils:get_env(epublish, publish_type)
    }.

get_slice_num(TopicSize, SliceSize) ->
    case TopicSize rem SliceSize of
        0 ->
            TopicSize div SliceSize;
        _ ->
            TopicSize div SliceSize + 1
    end.

generate_topic_slice_lists(SliceNum, TopicSize, SliceSize) ->
    SliceNum1 = SliceNum - 1,
    lists:foldl(fun(Index, ACC) ->
        case Index of
            SliceNum1 ->
                [{Index * SliceSize, TopicSize -1}] ++ ACC;
            _  ->
                [{Index * SliceSize, Index * SliceSize + SliceSize -1}] ++ ACC
        end end, [], lists:seq(0, SliceNum1)).

check_publish_result(SliceNum, Result) ->
    ExpectedResult = lists:duplicate(SliceNum, ok),
    case Result of
        ExpectedResult ->
            ok;
        _ ->
            {error, publish_failed}
    end.

save_msg_list(PublishUidLists, PublishArgs, EpublishMeta) ->
    QoS = PublishArgs#epublish_args.qos,
    MessageId = PublishArgs#epublish_args.message_id,
    case QoS > ?QOS_0 of
        true ->
            [epublish_storage:save_msgid_list(Uid, MessageId, PublishArgs, EpublishMeta) || Uid <- PublishUidLists]; %% maybe it need to be currency
        false ->
            ignore
    end.
