.PHONY: all deps clean release

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/epublish

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/epublish/bin/epublish start

console: generate
	./rel/epublish/bin/epublish console

foreground: generate
	./rel/epublish/bin/epublish foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s epublish
